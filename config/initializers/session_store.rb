# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_shortlink_session',
  :secret      => '612517598a10abceb05fbc4a2738d27f979063694c91687bdd8ecdc0fdf45e1d266a86fcad1dd0b5d7e24606d1a4474ca83b4653d246864b051176caac8e0dc0'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
